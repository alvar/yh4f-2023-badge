# Badge

![YH4F 2023 Badge w/ RGB colors](./contrib/animation.gif)


## Hardware

The component list is very short:

- Two parts of acryl glass
- Arduino Nano-_ish_ microcontroller
- WS2812B LED strip
- USB C cable
- Some jumper wires
- Screws with nuts

### Assembly Instructions

![Assembly Sketch](./contrib/sketch.jpg)

1. Cut of a long enough part of the LED strip to match the acryl glass' long side.
   Always make a clean cut at the soldered joint points and try not to steal the next person's precious soldering space.
2. Find the right end to start soldering: left of the leftmost right pointing arrow (`▶`).
   The direction of the addressed LEDs follows the arrow's direction.
3. Cut some jumper wires in half to end up with three wires each with one "female" end and one open wire.
   Prepare your wire's ends by cutting them open and applying a bit of soldering paste on it.
4. Solder three wires to your LED strip
   - one at the _+5V_ connection for the voltage,
   - one at _DIN_ for the data, and
   - one at _GND_ for the ground connection.
5. Connect the three wires to your microcontroller:
   - _+5V_ goes to _5V_,
   - _DIN_ goes to _D5_, and
   - _GND_ goes to _GND_ (doesn't matter which one).
6. Remove the film of the LED strip's back and stick it on one acryl glass.
7. Lock your Arduino Nano in place, e.g., with some additional glue.
8. Add the second acryl glass plate and tighten it with some screws.


## Build The Example Software


### Install PlatformIO

To compile and flash the software, [PlatformIO](https://platformio.org/) will be used.
Thus, start with installing PlatformIO if you haven't already done so.

There are [multiple flavours](https://platformio.org/install/integration) of PlatformIO:

- [PlatformIO Core - just the necessary CLI tools](https://platformio.org/install/cli) - and
- [PlatformIO IDE - a full-blown VSCode based IDE](https://platformio.org/install/ide)

Install whatever you prefer most, this manual continues with _PlatformIO Core_.


### Compile and Flash

The actual code resides in the `./src` directory.
For starters, a [FastLED rainbow animation copied from their examples](https://github.com/FastLED/FastLED/blob/3.6.0/examples/Pride2015/Pride2015.ino) lies there.

To compile the code, use PlatformIO's command line tool `pio`:
```
$ pio run
```

If there are any errors, first try to read and understand them and finally fix them.

After your code compiles, connect the Arduino Nano with the USB C cable to your machine and flash the code.
```
$ pio run --target upload
```

If this fails, try to understand the error:

- Maybe your board has an old bootloader and you need to change the `upload_speed` to `57600` in the [platformio.ini file](./platformio.ini).

- Maybe your user is not privileged enough, which can be fixed under Linux by the [documented udev rules](https://docs.platformio.org/en/latest/core/installation/udev-rules.html).
Otherwise, try it again with evaluated privileges (add `sudo`) which, however, I don't want to recommend anyone.

Sometimes the microcontroller just doesn't feel like being flashed right now.
Thus, give it a second try or reconnect it.

When you have successfully flashed your Arduino, the LED strip will be illuminated after an initial delay of three seconds.

Now it's up to you to hack the software for the better.
As a first change, you might wanna alter `NUM_LEDS` to the amount of LEDs on your strip.
